package com.example.josedeskt.convertidortemperatura;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActividadPrincipal extends AppCompatActivity {

    Button convetirAF, convetirAC;
    EditText magnitud, resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);


        //Declaramos
        //Editor de texto donde se pondra la cantidad a convetir.
         magnitud = (EditText) findViewById(R.id.EDTcantidad);
        //Editor de texto donde se pondra el resultado de la conversion.
         resultado = (EditText) findViewById(R.id.EDTresultado);
          resultado.setEnabled(false);
         //Declaracion de los 2 botones que usaremos para las respectivas conversiones.

        //Declaramos el primer boton que se encargara de convetir de Fanrenheit a centigrados
        convetirAC = (Button)findViewById(R.id.BTNConvetirAC);
        //Se inicializa el boton
        convetirAC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Metodo que se ejecutara cuando pulsamos el boton.
                //Cada vez que pulsamos el boton, se borra el resultado anterior.
                resultado.setText("");
                //Obtenemos el valor que el usuario escribio y se guarda en la variable n.
                double n = Double.parseDouble(magnitud.getText().toString());
                //Se procede a relizar la operacion aritmetica.
                double r = (n - 32) * 5/9;

                //Luego se escribe en el TxT del resultado.
                resultado.setText(String.valueOf(r +" ºC"));

            }
        });
        //Declaramos el segundo boton que se encargara de convetir de centigrados a Fanrenheit
        convetirAF = (Button) findViewById(R.id.BTNconvertirAF);
        //Se inicializa el boton
        convetirAF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Metodo que se ejecutara cuando pulsamos el boton.
                //Cada vez que pulsamos el boton, se borra el resultado anterior.
                resultado.setText("");
                //Obtenemos el valor que el usuario escribio y se guarda en la variable n.
                double n = Double.parseDouble(magnitud.getText().toString());
                //Se procede a relizar la operacion aritmetica.
                double r = (n * 9/5) + 32;

                resultado.setText(String.valueOf(r+" ºF"));
            }
        });


    }
}
